import { createRouter, createWebHistory } from "vue-router";
import ListView from "@/views/ListView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      alias: "/list",
      component: () => import("../views/ListView.vue"),
    },
    {
      path: "/downloads/",
      name: "downloads",
      component: () => import("../views/DownLoadsView.vue"),
    },
    {
      path: "/download/:id",
      name: "download-detail",
      component: () => import("../views/DownLoadView.vue"),
    },
  ],
});

export default router;
