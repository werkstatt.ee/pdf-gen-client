import { io } from "socket.io-client";
const socket = io(import.meta.env.VITE_DOWNLOADS_SERVER_URL as string, {});

export const useSocketIO = () => {
  return {
    socket,
  };
};
