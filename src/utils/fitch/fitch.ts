/* eslint-disable @typescript-eslint/no-explicit-any */
export interface HttpOptions {
  baseURL?: string;
  transformRequest?: FitchTransformRequest[];
  transformResponse?: FitchTransformResponse[];
  headers?: HeadersInit;
}

export interface HttpResponse<T> extends Response {
  parsedBody?: T;
}

export interface FitchRequest {
  url: URL;
  requestArgs: RequestInit;
  body?: JsonObject;
}

export interface FitchTransformRequest {
  (request: FitchRequest): FitchRequest;
}

export interface FitchTransformResponse {
  <T>(response: HttpResponse<T>): HttpResponse<T>;
}

export interface JsonObject {
  [k: string]: any;
}

export class Fitch {
  private readonly baseURL: string = "";
  private readonly transformRequest: (FitchTransformRequest | null)[] = [];
  private readonly transformResponse: (FitchTransformResponse | null)[] = [];
  private readonly headers: HeadersInit = [];

  constructor(options?: HttpOptions) {
    if (options && Object.keys(options).length > 0) {
      this.baseURL = options.baseURL || "";
      this.transformRequest = options.transformRequest || [];
      this.transformResponse = options.transformResponse || [];
      this.headers = { Accept: "application/json", "Content-Type": "application/json", ...options.headers };
    }
  }

  public addTransformRequest(transformer: FitchTransformRequest): number {
    this.transformRequest.push(transformer);
    return this.transformRequest.length - 1;
  }

  public removeTransformRequest(index: number) {
    if (this.transformRequest[index]) {
      this.transformRequest[index] = null;
    }
  }

  public addTransformResponse(transformer: FitchTransformResponse): number {
    this.transformResponse.push(transformer);
    return this.transformRequest.length - 1;
  }

  public removeTransformResponse(index: number) {
    if (this.transformResponse[index]) {
      this.transformResponse[index] = null;
    }
  }

  private async http<T>(path: string, args: JsonObject): Promise<T> {
    const url = new URL(this.baseURL + path);

    return new Promise((resolve, reject) => {
      let response: HttpResponse<T>;
      let newArgs: FitchRequest = {
        url: url,
        requestArgs: {
          method: args.method,
          body: JSON.stringify(args.body) || null,
          headers: { ...this.headers, ...args.headers },
        },
        body: args.body || null,
      };

      this.transformRequest.forEach((transformer) => {
        if (transformer) {
          newArgs = transformer(newArgs);
        }
      });
      const stringUrl = newArgs.url.toString();
      const request = new Request(stringUrl, newArgs.requestArgs);

      fetch(request)
        .then((res) => {
          response = res;
          return res.text();
        })
        .then((text) => {
          let body: T;
          try {
            body = text ? JSON.parse(text) : {};
          } catch (e) {
            body = {} as T;
          }
          response.parsedBody = body;
          this.transformResponse.forEach((transformer) => {
            if (transformer) {
              response = transformer(response);
            }
          });

          if (response.ok) {
            resolve(response.parsedBody);
          } else {
            reject(response.parsedBody);
          }
        })
        .catch((err) => {
          reject(err);
        });
    });
  }

  public async get<T>(path: string, args?: JsonObject): Promise<T> {
    return this.http<T>(path, { ...args, method: "GET" });
  }

  public async delete<T>(path: string, args?: JsonObject): Promise<T> {
    return this.http<T>(path, { headers: { ...args }, method: "DELETE" });
  }

  public async post<T>(path: string, body?: any, args?: JsonObject): Promise<T> {
    return this.http<T>(path, { headers: { ...args }, method: "POST", body: body });
  }

  public async put<T>(path: string, body: any): Promise<T> {
    return this.http<T>(path, { method: "PUT", body: body });
  }

  public async patch<T>(path: string, body: any, args?: JsonObject): Promise<T> {
    return this.http<T>(path, { ...args, method: "PATCH", body: body });
  }
}
