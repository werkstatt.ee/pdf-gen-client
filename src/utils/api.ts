import { Fitch } from "./fitch/fitch";
import { transformToSnake, transformToCamel } from "./fitch/helpers/transformers";

const isValidUrl = (urlString?: string): urlString is string => {
  if (!urlString) return false;
  try {
    return Boolean(new URL(urlString));
  } catch (e) {
    return false;
  }
};

const createBaseUrl = (urlString?: string): string => {
  if (!urlString || !isValidUrl) {
    throw new Error("Invalid API_URL, please check .env");
    return "";
  }
  if (urlString?.charAt(urlString.length - 1).match("/")) {
    return urlString.slice(urlString.length - 1);
  }
  return urlString;
};

export const jokesApi = new Fitch({
  baseURL: `${createBaseUrl(import.meta.env.VITE_JOKE_SERVER_URL as string)}`,
  transformRequest: [transformToSnake],
  transformResponse: [transformToCamel],
});

export const downloadsApi = new Fitch({
  baseURL: `${createBaseUrl(import.meta.env.VITE_DOWNLOADS_SERVER_URL as string)}`,
  transformRequest: [transformToSnake],
  transformResponse: [transformToCamel],
});
