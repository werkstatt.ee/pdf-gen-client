export const getFileUrl = (path: string): string => {
  return `${import.meta.env.VITE_DOWNLOADS_SERVER_URL as string}/downloads/${path}`;
};
