import { defineStore } from "pinia";
import { downloadsApi, jokesApi } from "@/utils/api";

interface State {
  categories: string[];
  loadingCategories: boolean;
  jokes: Joke[];
  loadingJokes: boolean;
  downloads: Download[];
  loadingDownloads: boolean;
  currentDownLoad?: Download;
  preparingDownload: boolean;
}

export const useJokesStore = defineStore("jokes", {
  state: (): State => {
    return {
      categories: [],
      loadingCategories: false,
      jokes: [],
      loadingJokes: false,
      downloads: [],
      loadingDownloads: false,
      preparingDownload: false,
      currentDownLoad: undefined,
    };
  },
  getters: {
    getCurrentDownload: (state) => (state.currentDownLoad ? state.currentDownLoad : null),
  },
  actions: {
    async fetchCategories() {
      this.loadingCategories = true;
      try {
        const categories = await jokesApi.get<string[]>("/jokes/categories");
        this.categories = categories;
      } catch (error) {
        console.log("Error loading categories");
      }
      this.loadingCategories = false;
    },
    async fetchJokes(count: number, category?: string) {
      let path = "/jokes/random";
      if (category) {
        path = `${path}?category=${category}`;
      }
      this.loadingJokes = true;
      this.jokes = [];

      for (let index = 0; index < count; index++) {
        try {
          const joke = await jokesApi.get<Joke>(path);
          this.jokes.push(joke);
        } catch (error) {
          console.log(`Error loading joke #${index}`);
        }
      }
      this.loadingJokes = false;
    },
    async fetchDownloads() {
      this.loadingDownloads = true;
      this.downloads = [];
      try {
        const downloads = await downloadsApi.get<Download[]>("/downloads");
        this.downloads = downloads;
      } catch (error) {
        console.log(`Error loading downloads: ${error}`);
      }
      this.loadingDownloads = false;
    },
    async fetchDownload(id: string) {
      this.preparingDownload = true;
      this.downloads = [];
      try {
        const download = await downloadsApi.get<Download>(`/downloads/${id}`);
        this.currentDownLoad = download;
      } catch (error) {
        console.log(`Error loading downloads: ${error}`);
      }
      this.preparingDownload = false;
    },
    async generateDownload(jokeIds: string[]) {
      this.preparingDownload = true;
      this.currentDownLoad = undefined;
      try {
        const download = await downloadsApi.post<Download>("/downloads", { jokeId: jokeIds });
        console.log("setting current download");
        this.currentDownLoad = download;
      } catch (error) {
        console.log(`Error loading downloads: ${error}`);
      }
      this.preparingDownload = false;
      return this.currentDownLoad;
    },
    async updateDownLoad(download: Download) {
      if (this.currentDownLoad && this.currentDownLoad.id === download.id) {
        this.currentDownLoad = download;
      }
      const found = this.downloads.findIndex((d) => d.id === download.id);
      if (found >= 0) {
        this.$patch((state) => {
          state.downloads[found] = download;
        });
      } else {
        this.$patch((state) => {
          state.downloads.push(download);
        });
      }
    },
  },
});

export interface Joke {
  categories: string[];
  createdAt: string;
  iconUrl: string;
  id: string;
  updatedAt: string;
  url: string;
  value: string;
}

export interface Download {
  id: number;
  description: string;
  filename?: string;
  isGenerated: boolean;
  updatedAt: number;
  createdAt: number;
}
